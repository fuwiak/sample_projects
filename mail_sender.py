# -*- coding: utf-8 -*-


import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders


def attachment(file_path,filename,tresc_wiadomosci):
    msg = MIMEMultipart()
    msg.attach(MIMEText(tresc_wiadomosci, 'plain'))
    attachment = open(file_path, "rb")
    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment).read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
    msg.attach(part)
    msg = msg.as_string()
    return msg
    

haslo = 'pass'
adres = 'mail@gmail.com'
body1 = "wiadomosc1" 
name1 = "file.txt"
path1 = "/path/file.txt"
body2 = "wiadomosc2" 
name2 = "rep_usr.txt"
path2 = "path.txt"


adres_doc = 'who.knows@gmail.com'
 

server = smtplib.SMTP('smtp.gmail.com', 587)
server.starttls()
server.login(adres, haslo)
 
#msg = "YOUR MESSAGE!"
MSG1 = attachment(path1,name1, body1)
MSG2 = attachment(path2,name2, body2)
server.sendmail(adres, adres_doc, MSG1)
server.sendmail(adres, adres_doc, MSG2)

server.quit()
