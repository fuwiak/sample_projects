import Tweets
import sqlalchemy
from sqlalchemy.orm import sessionmaker


class TweetsDatabase:

    def __init__(self, databasename="tweets.db"):
        self.engine = sqlalchemy.create_engine('sqlite:///%s' % databasename, echo=False)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        self.tweet = Tweets.Tweet

    def print_database(self):
        for tweet in self.session.query(self.tweet):
            print("""
            id  :  %s
            rate:  %s
            User:  %s
            RUser: %s
            text:  %s
            """ % (tweet.get_id(), tweet.rating, tweet.get_screen_name(), tweet.get_retweeted_screen_name(), tweet.get_text()))

    def get_tweet(self, id_tweet):
        return self.session.query(self.tweet).filter(self.tweet.id_tweet == id_tweet).scalar()

    def commit(self):
        self.session.commit()

    def add_tweets(self, tweets):
        if tweets is list:
            self.session.add_all(tweets)
        else:
            self.session.add_all([tweets])
        self.commit()


if __name__ == "__main__":
    db = TweetsDatabase()
    db.print_database()


