#!/usr/bin/env python2

import json
import tweepy


def test1():
    with open('streamdata.txt', 'r') as fh:
        lines = [l.decode('utf-8') for l in fh.readlines() if len(l)>0]
    
    json_lines = map(json.loads, lines)
    print(json_lines)
    

def test2(nazwa_pliku):
    tweets_data = []
    #nazwa_pliku = 'streamdata.txt'
    tweets_file = open(nazwa_pliku, "r")
    for line in tweets_file:
        try:
            line = line.decode('utf-8')
            tweet = json.loads(line)
            tweets_data.append(tweet)
        except Exception as e:
            print(line)
            print(e)
            break
    
    return tweets_data



#convert from tweepy.models.Status to json
@classmethod 
def parse(cls, api, raw):
    status = cls.first_parse(api, raw)
    setattr(status,'json',json.dumps(raw))
    return status
    
    

    
    




