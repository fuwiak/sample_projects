# -*- coding: utf-8 -*-
"""

@author: Pawel
"""
import time
import load as ld

def autorzy(auth, hashtag, day1, day2,lang_code, count_authors):
    from datetime import date
    from datetime import timedelta
    from tweepy import API
    from tweepy import Cursor
    from collections import Counter
    import heapq
    authors= []
    api = API(auth)
    for tweet in Cursor(api.search,q=hashtag,since=day1, until=day2,lang=lang_code).items(): 
        authors.append(tweet.author.screen_name)
        #time.sleep(60)
    n_biggest = count_authors
    value = heapq.nlargest(n_biggest,Counter(autorzy).values())
    return authors, value


def autor_dla_wybranej_liczby(autorzy, wybrana_wartosc):
    from collections import Counter
    for autor, ile in Counter(autorzy).items():
        if ile == wybrana_wartosc:
            return autor
            
def average_value(user_name):
    twity = ld.test2('fetched_tweets.txt') # twity[0]<-- first twit etc
    for i in range(len(twity)):
        if twity[i]['user']['screen_name'] == user_name:
            number_all_posts=twity[i]['user']['statuses_count']
            kk = i


    retwit=twity[kk]['retweet_count']
    lajk=twity[kk]['favorite_count']
    average_likes = lajk/float(number_all_posts)
    average_retwit = retwit/float(number_all_posts)
    return average_likes, average_retwit, 
    
def index_of_retwit(number_of_retwit):
	if number_of_retwit ==1:
		return 1
	elif number_of_retwit ==1.5:
		return 1.5
	else:
		return (11.0+number_of_retwit)/8
  
  
def index_of_valueable(user_name, twity):
    for i in range(len(twity)):
        if twity[i]['user']['screen_name'] == user_name:
            following = twity[i]['user']['friends_count']
            followers = twity[i]['user']['followers_count']
            likes = twity[i]['user']['favourites_count']
            count_twit = twity[i]['user']['statuses_count']
    kk = (float(followers)/following)*likes/count_twit
    return kk
        
    
def licz_tweets(api, username, start, end):
    import tweepy, datetime, time
    start = datetime.datetime.strptime(start, "%Y-%m-%d")
    end = datetime.datetime.strptime(end, "%Y-%m-%d")
    number_days = abs((start-end).days)
    
    page = 1
    deadend = False
    twity = []
    while True:
        tweets = api.user_timeline(username, page = page)
        for tweet in tweets:
			if (datetime.datetime.now() - tweet.created_at).days < 1:
				twity.append(tweet.text.encode("utf-8"))
			else:
				deadend = True
				return twity
		
                
        if not deadend:
            page+=1
            #time.sleep(500)    
    
    
    
