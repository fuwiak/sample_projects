#Import the necessary methods from tweepy library
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream

# Variables that contains the user credentials to access Twitter API
access_token = "473240935-fy5Fgp0IF9Q2cCtSviNznCcm7wrVa53GCeNybZjn"
access_token_secret = ""
consumer_key = "O8dNs7PiBaYeVRa1okH7dlkYH"
consumer_secret = ""

# Open file to save data
data_file = open("streamdata.txt", "w")

# This is a basic listener that just prints received tweets to stdout.
class StdOutListener(StreamListener):

    def on_data(self, data):
        print(data)
        data_file.write(data.encode('utf-8', 'replace'))  #XXX: <--- encode data
        with open('fetched_tweets.txt','a') as tf:
            tf.write(data)
        return True

    def on_error(self, status):
        print(status)


if __name__ == '__main__':

    # This handles Twitter authetification and the connection to Twitter Streaming API
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, StdOutListener())

    # This line filter Twitter Streams to capture data by the keywords: 'python', 'javascript', 'ruby'
    #track = ['cyberwar','hacker', '#hacker','cracker','#cracker','#locky', 'locky', 'ramsoware', '#ramsomware', 'cybersecurity', '#cybersecurity', 'crypto','#crypto', '#cryptography', 'exploit', '#exploit']
    track = ['malware', '#malware']
    stream.filter(track=track)
