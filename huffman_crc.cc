#include <cstdlib>
#include <iostream>
#include <fstream>
#include <ostream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <conio.h>
#include <math.h>

/* HUFFMAN AND CRC CODING */

using namespace std;
struct tree{
    int father;
    int left;
    int right;
};
struct table_of_encoding{
    int symbol;
    string kod;
    int len_code;
};
struct maxin{
    int maxx;
    int minn;
    int endd;
    int startt;
};
int str2int(string text){
    int number;
    stringstream conversion(text);
    conversion>>number;
    return number;
}
void sort_table_of_encoding(int tab[],int sign[], int n){
    int temp;
    for(int j=n-1; j>0; j--)
        for (int i=0; i<j; i++){
            if (tab[i]>tab[i+1]){
            swap(tab[i], tab[i+1]);
            swap(sign[i], sign[i+1]);
        }
    }
}
int find_bayty(int signi[],int tab[],int counter,string s){
    char c,sign[256];
    int i,flag;
    ifstream input;
    input.open(s.c_str(),ifstream::binary);
		while (!input.eof()){
			c=input.get();
			if(!input.eof()){
			i=0;
			flag=1;
			while(flag && i<counter+1){
				if(sign[i]==c){
					tab[i]++;
					flag=0;
				}
				else{
					if(i==counter){
						sign[counter]=c;
						signi[counter]=(signed char)sign[counter];
						tab[counter]++;
						counter++;
						flag=0;
                    }
                }
            i++;
            }
        }}
    input.close();
    sort_table_of_encoding(tab,signi,counter);
    return counter;
}
int b_tree(struct tree Tree_tab[],int signi[],int tab[],int counter){
  	int i=0;
  	int temp=0;
    while(i<counter){
        Tree_tab[i].father=257+i;
        Tree_tab[i].left=signi[i];
        Tree_tab[i].right=signi[i+1];
        signi[i+1]=257+i;
        tab[i+1]+=tab[i];
        tab[i]=0;
        sort_table_of_encoding(tab,signi,counter);
        i++;
    }
    return (i);
}
int b_table_of_encoding(struct table_of_encoding tabk[],struct drz Tree_tab[],int counter){
    struct table_of_encoding table_of_coding[512];
    int i=0,j=0,k=0;
    if(counter>1){
        table_of_coding[0].symbol=Tree_tab[counter-2].father;
        table_of_coding[0].kod="1";
        table_of_coding[0].len_code=table_of_coding[0].kod.size();
        for(i=0;i<counter-1+counter;i++){
            for(k=0;k<counter-1;k++){
                if(table_of_coding[i].symbol==Tree_tab[counter-2-k].father){
                    table_of_coding[j+1].symbol=Tree_tab[counter-2-k].left;
                    table_of_coding[j+2].symbol=Tree_tab[counter-2-k].right;
                    table_of_coding[j+1].kod=table_of_coding[i].kod+"0";
                    table_of_coding[j+2].kod=table_of_coding[i].kod+"1";
                    table_of_coding[j+1].len_code=table_of_coding[j+1].kod.size();
                    table_of_coding[j+2].len_code=table_of_coding[j+2].kod.size();
                    j=j+2;
                    break;
                }
            }
        }
        j=0;
        for(i=0;i<2*counter-1;i++){
            if(table_of_coding[i].symbol<257){
                tabk[j].symbol=table_of_coding[i].symbol;
                tabk[j].kod=table_of_coding[i].kod;
                tabk[j].len_code=table_of_coding[i].len_code;
                j++;
            }
        }
        return j;
    }
    else{
        tabk[0].symbol=Tree_tab[0].left;
        tabk[0].kod="1";
        tabk[0].len_code=1;
        return 1;
    }
}

int score_crc32(char bayt,int crc32){
    bool jest=false;
    int i;
    char tmpb;
    int tmp;
    for(i=0;i<8;i++){
        if(crc32&0x80000000)jest=true;
        else jest=false;
        crc32<<=1;
        crc32&=0xfffffffe;
        tmpb=bayt>>(7-i);
        tmpb=tmpb&0x01;
        crc32|=(int)tmpb;
        if(jest)crc32^=0x04c11db7;
    }
    return crc32;
}

int strbin2int(string bin){
	int dec=0,size1,size2,i=0;
	size1=size2=bin.length()-1;
	if(bin[i]=='1')dec-=pow(2,size2--);
	else size2--;
	for(i=1;i<=size1;++i)
        if(bin[i]=='1')dec+=pow(2,size2--);
        else size2--;
	return dec;
}
int strbin22int(string bin){
	int dec=0,size1,size2,i=0;
	i=bin.length()-1;
	string tmp="0";
	for(i;i<32;i++){
        tmp+="0";
	}
    tmp+=bin;
    bin=tmp;
    size1=size2=bin.length()-1;
	for(i=1;i<=size1;++i)
        if(bin[i]=='1')dec+=pow(2,size2--);
        else size2--;
	return dec;
}
string no_spaces(string s){
    int i,dot;
    string z;
    for(i=0;i<s.size();i++){
        if(s[i]=='.')dot=i;
    }
    if(dot!=0)z.assign(s,0,dot);
    else z=s;
    return z;
}

string s_roz(string s){
    int i,dot;
    string z;
    for(i=0;i<s.size();i++){
        if(s[i]=='.')dot=i;
    }
    if(dot!=0)z.assign(s,dot,s.size());
    return z;
}
int save_to_comp(string s,string z,struct table_of_encoding table_of_coding[],int counter,int crc32){
    int i,tmp,tmp2,rett;
    string kod,kod2;
    ofstream output;
    ifstream input;
    output.open(z.c_str(),ifstream::binary);
    output.put((char)0);
    output.write((char*)&crc32,sizeof(int));
    output.put((unsigned char)counter-1);
    for(i=0;i<counter;i++){
        output.put((char)table_of_coding[i].symbol);
        tmp=strbin22int(table_of_coding[i].kod);
        output.write((char*)&tmp,sizeof(int));
    }

    input.open(s.c_str(),ifstream::binary);
    while(!input.eof()){
        tmp2=(signed char)input.get();
        if(input.eof())break;
        for(i=0;i<counter;i++){
            if(tmp2==table_of_coding[i].symbol)kod+=table_of_coding[i].kod;
        }
        tmp=kod.size();
        while(tmp>=8){
            kod2.assign(kod.c_str(),8);
            kod.assign(kod.c_str(),8,tmp-8);
            tmp2=strbin2int(kod2);
            output.put((char)tmp2);
            tmp=kod.size();
        }
    }
    i=kod.size();
    tmp=i;
    for(i;i<8;i++){
        kod+="0";
    }
    tmp2=strbin2int(kod);
    output.put((char)tmp2);
    rett=output.tellp();
    output.seekp(0);
    output.put((char)8-tmp);
    output.close();
    input.close();
    return rett;
}
string int2bin(int number){
    string kod;
    while(number){
        if(number%2)kod="1"+kod;
        else kod="0"+kod;
        number=number/2;
    }
    return kod;
}
string int22bin(int number){
    string kod;
    int i;
    int temp=0;
    for(i=0;i<8;i++){
        if(number&0x80) temp=1;
        else jest=0;
        number<<=1;
        if(temp==1)kod=kod+"1";
        else kod=kod+"0";
    }
    return kod;
}
struct maxin read_tab(string s,struct table_of_encoding table_of_coding[],int counter,int seek){
    struct maxin minax;
    int i,tmp;
    ifstream input;
    input.open(s.c_str(),ifstream::binary);
    input.seekg(0,input.end);
    minax.endd=input.tellg();
    input.close();
    input.open(s.c_str(),ifstream::binary);
    input.seekg(seek,input.beg);
    for(i=0;i<counter;i++){
        table_of_coding[i].symbol=(signed char)input.get();
        input.read((char*)&tmp,4);
        tmp>>=1;
        table_of_coding[i].kod=int2bin(tmp);
        if(i==0)minax.minn=table_of_coding[i].len_code=table_of_coding[i].kod.size();
        else if(i==counter-1) minax.maxx=table_of_coding[i].len_code=table_of_coding[i].kod.size();
        else table_of_coding[i].len_code=table_of_coding[i].kod.size();
    }
    minax.startt=input.tellg();
    minax.endd-=input.tellg();
    input.close();
    return minax;
}

int decompression(string s,string e,struct maxin minax,int counter,int bonus,struct table_of_encoding table_of_coding[]){
    int i=0,found=0,j=0,tmp,tmp2,symbol;
    string kod,kod2;
    ifstream input;
    input.open(s.c_str(),ifstream::binary);
    input.seekg(minax.startt,input.beg);
    ofstream output;
    output.open(e.c_str(),ifstream::binary);
    while(minax.endd){
        if(!found&&(kod.size()<minax.maxx)){
            tmp=(unsigned char)input.get();
            kod+=int22bin(tmp);
            minax.endd--;
        }
        if(minax.minn>8)j=8; else j=minax.minn-1;
        tmp2=kod.size();
        while(j<=tmp2&&!found){
            i=0;
            j++;
            while(i<counter&&!found){
                    kod2.assign(kod.c_str(),j);
                if(kod2==table_of_coding[i].kod){
                    found=1;
                    symbol=(char)table_of_coding[i].symbol;
                }
                i++;
            }
        }
        if(found){
            kod2.assign(kod.c_str(),j);
            kod.assign(kod.c_str(),j,tmp2-j);
            output.put(symbol);
            found=0;
        }
    }
    kod.assign(kod.c_str(),kod.size()-bonus);
    while(kod.size()){
        if(minax.minn>8)j=8; else j=minax.minn-1;
        tmp2=kod.size();
        while(j<=tmp2&&!found){
            i=0;
            j++;
            while(i<counter&&!found){
                    kod2.assign(kod.c_str(),j);
                if(kod2==table_of_coding[i].kod){
                    found=1;
                    symbol=(char)table_of_coding[i].symbol;
                }
                i++;
            }
        }
        if(found){
            kod2.assign(kod.c_str(),j);
            kod.assign(kod.c_str(),j,tmp2-j);
            output.put(symbol);
            found=0;
        }
    }
    tmp=output.tellp();
    input.close();
    output.close();
    return tmp;
}

int main(){
    struct tree Tree_tab[512];
    struct table_of_encoding table_of_coding[256];
    char bayt,symbol;
    string z,s,tmps,e,kod,kod2;
	int i,suma=0,counter=0,tab[256],signi[256],finish=0,crcstare,bonus,tmp,found=0,tmp2,minn=0,maxx=0,endd,flag=0,mode=1;
	int crc32=0;
	while(mode){
		cout<<endl<<"[1] - coding "<<endl;
		cout<<"[2] - decoding "<<endl;
		cout<<"[0] - endd"<<endl;
		cin>>mode;
		if (mode==0) return 0;
		cout<<" Name of file "<<endl;
		cin>>s;
	    if(mode==1){
	        ifstream spr;
	        spr.open(s.c_str(),ifstream::binary);
	        if(spr.is_open()){
	            cout<<" File in compression "<<s<<endl<<endl;
	            cout<<"Counting CRC32."<<endl;
	            ifstream input;
	            input.open(s.c_str(),ifstream::binary);
	            while(input.good()){
	                bayt=input.get();
	                crc32=score_crc32(bayt,crc32);
	            }
	            for(i=0;i<4;i++){
	                crc32=score_crc32(0,crc32);
	            }
	            input.close();
	            cout<<"CRC32= "<<crc32<<endl<<endl;
	            cout<<" "<<endl;
	            counter=find_bayty(signi,tab,counter,s);
	            for(i=0;i<=counter-1;i++) sum=sum+tab[i];
	            cout<<"table_of_encoding: "<<sum<<endl<<"types of elements"<<counter<<endl<<endl;
	            cout<<"Create treee "<<endl;
	            tmp=b_tree(Tree_tab,signi,tab,counter);
	            cout<<""<<tmp<<endl<<endl;
	            cout<<"Create table_of_coding"<<endl;
	            tmp=b_table_of_encoding(table_of_coding,Tree_tab,counter);
	            cout<<"Size table_of_coding:"<<tmp<<endl<<endl;
	            z=no_spaces(s);
	            z+=s_roz(s);
	            z+="1";
	            cout<<"File compression "<<endl;
	            tmp=save_to_comp(s,z,table_of_coding,counter,crc32);
	            cout<<"byte saved to ->> *.huff : "<<tmp<<endl<<endl;
	            cout<<"file_name "<<z<<endl;
	            cout<<"percent compression "<<(float)tmp/suma*100<<" %"<<endl;
	            cout<<"Saved CRC32! "<<crc32<<endl;
	        }
	        else cout<<"No file to compression"<<endl<<endl;
	        finish=1;
	        spr.close();
	    }
	    else if(mode==2){
	        struct maxin minax;
	        ifstream spr;
	        spr.open(s.c_str(),ifstream::binary);
	        if(spr.is_open()){
	            cout<<"File to compression "<<s<<endl<<endl;
	            ifstream input;
	            input.open(s.c_str(),ifstream::binary);
	            bonus=input.get();
	            cout<<" CRC32."<<endl;
	            input.read((char*)&crcstare,4);
	            cout<<"readed CRC32: "<<crcstare<<endl<<endl;
	            counter=(unsigned char)input.get();
	            counter++;
	            tmp=input.tellg();
	            input.close();
	            cout<<"Show table_of_coding."<<endl;
	            minax=read_tab(s,table_of_coding,counter,tmp);
	            cout<<"number table_of_coding: "<<counter<<endl<<endl;
	            e=no_spaces(s);
	            e+="_decoded";
	            e+=s_roz(s);
	            e.erase(e.length()-1,1);
	            cout<<"decompression file"<<endl;
	            tmp=decompression(s,e,minax,counter,bonus,table_of_coding);
	            cout<<"decompression has ende, number baytow file *.* : "<<tmp<<s_roz(s)<<endl<<endl;
	            crc32=0;
	            cout<<"count CRC32 file after decompression"<<endl;
	            input.open(e.c_str(),ifstream::binary);
	            while(input.good()){
	                bayt=input.get();
	                crc32=score_crc32(bayt,crc32);
	            }
	            for(i=0;i<4;i++){
	                crc32=score_crc32(0,crc32);
	            }
	            input.close();
	            cout<<"name file after decompression "<<e<<endl;
	            cout<<"size name file after decompression "<<tmp<<endl;
	            if(crc32==crcstare)cout<<"crc ok "<<crc32<<"="<<crcstare<<endl<<endl;
	            else cout<<"crc not ok "<<crc32<<"!="<<crcstare<<endl<<endl;
	            cout<<"End decompression"<<endl<<endl;
	        }
	            else cout<<"No file to compression"<<endl<<endl;
	            finish=1;
	    }
	    else if(!finish){
	        cout<<"No mode!"<<endl;
	        cout<<"Enabled mode"<<endl;
	      ` cout<<endl<<"[1] - coding "<<endl;
			cout<<"[2] - decoding "<<endl;
		    cout<<"[0] - endd"<<endl;
	        cout<<"Choose right mode"<<endl;
	        finish=1;
	    }
    }
return 0;
}
