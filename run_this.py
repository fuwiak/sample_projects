# -*- coding: utf-8 -*-
"""
Created on Mon Aug 29 10:39:12 2016

@author: pawel
"""

from parse_html import extract_login_fromRT as ex
from parse_html import return_api
import re
import time

api = return_api()
url = 'fetched_tweets.txt'


class Start:
    def __init__(self, url):
        self.url = url
        self.users = ['abuse_ch', 'hasherezade', 'circl_lu', 'j00ru', 'lcamtuf', 'rootkovska'] # experts
        self.twit_id = []
        self.screen_name = []
        self.liczba_lajkow = []
        self.liczba_retwitow = []


    def read_file(self,url):
        plik = open(url, 'r')
        plik = plik.readlines()
        plik = [pp.split('##') for pp in plik]
        return plik
        
    def stats(self, url):
        plik = self.read_file(self.url)
        twit_id = [plik[i][5].strip('\n') for i in range(len(plik))]
        screen_name = [plik[i][1][1:-1] for i in range(len(plik))]
        liczba_lajkow = [plik[i][3][:-2] for i in range(len(plik))]
        liczba_retwitow = [plik[i][4][:-2] for i in range(len(plik))]
        self.twit_id, self.screen_name, self.liczba_lajkow, self.liczba_retwitow = twit_id, screen_name, liczba_lajkow, liczba_retwitow 
        
        return twit_id, screen_name, liczba_lajkow, liczba_retwitow
        
    def read_file_json(self,url):
        import json
        plik = open(url, 'r')
        plik = plik.readlines()
        plik = [json.loads(plik[ii]) for ii in range(len(plik))]
        return plik
    
    def stats_json(self,url):
        plik = self.read_file_json(self.url)
        twit_id = [plik[ii]['id'] for ii in range(len(plik))]
        screen_name = [plik[ii]['user']['screen_name'] for ii in range(len(plik))]
        #liczba_lajkow = [plik[ii]['retweeted_status']['favorite_count'] for ii in range(len(plik))]
        #liczba_retwitow = [plik[ii]['retweeted_status']['retweet_count'] for ii in range(len(plik))]
        self.twit_id, self.screen_name = twit_id, screen_name

        return twit_id, screen_name
        
    def stats_json_fav_RT(self, url):
        ll = []
        RT = []
        plik = self.read_file_json(self.url)
        for i in range(len(plik)):
            if 'retweeted_status' in plik[i].keys():
                ll.append(plik[i]['retweeted_status']['favorite_count'])
                RT.append(plik[i]['retweeted_status']['retweet_count'])
            elif 'retweeted_status' in plik[i].keys() == False:
                ll.append(plik[i]['favorite_count'])
                RT.append(plik[i]['retweet_count'])
            else:
                ll.append(0)
                RT.append(0)
        self.liczba_lajkow, self.liczba_retwitow = ll, RT
        return self.liczba_lajkow, self.liczba_retwitow
            

    def create_rep(self,url):
        twit_id = self.stats_json(url)[0]        
        reputacja_twitow = dict()
        for i in range(len(twit_id)):
            reputacja_twitow[i] = 0
        return reputacja_twitow

    def screen_name_RT_and_link(self,url):
        twit_id = self.stats_json(url)[0] 
        wyniki = []
        try:    
            for i in range(len(twit_id)):
                link = 'https://twitter.com/anyuser/status/'+str(twit_id[i])
                loginy, link = ex(link,api)
                
                wyniki.append((loginy, link))
                time.sleep(2)
                print wyniki
        except Exception as e:
            print e.message
            pass
        return wyniki
        
    def update_rep(self):
        twit_id = self.stats_json(url)[0] 
        wyniki = self.screen_name_RT(twit_id) 
        reputacja_twitow = self.create_rep(self,url)
        for k in wyniki:
            for j in k[0]:
                if j in self.users:
                    pass
                    reputacja_twitow[re.findall('\d+', k[1])[0]]+=100
        return reputacja_twitow
        
def if_user_in_rank(logins_from_RT,users):
    import itertools
    yy = list(itertools.chain(*logins_from_RT))
    for uu in users:
        print uu in yy
        
        

if __name__ == "__main__":
    
    start  = Start(url)
    liczba_lajkow, liczba_retwitow = start.stats_json_fav_RT(url)
    twit_id, screen_name = start.stats_json(url)
    '''
    ss = time.time()
    screen_name_RT_and_link = start.screen_name_RT_and_link(url) # <----najdluzej wykonywujacy sie kawalek kodu
    kk = time.time()
    logins_from_RT = [screen_name_RT_and_link[i][0] for i in xrange(len(screen_name_RT_and_link))]
    if_user_in_rank(logins_from_RT, start.users)
    kk = time.time()
    print kk-ss
'''



