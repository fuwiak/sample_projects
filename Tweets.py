import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import Users
import RateModule
import json

engine = sqlalchemy.create_engine('sqlite:///tweets.db', echo=True)
Base = declarative_base(bind=engine)


class Tweet(Users.RateModule.Rate, Base):
    __tablename__='tweets'
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    id_tweet = sqlalchemy.Column(sqlalchemy.Integer)
    limit = sqlalchemy.Column(sqlalchemy.Integer)
    rating = sqlalchemy.Column(sqlalchemy.Integer)
    data = sqlalchemy.Column(sqlalchemy.String(50000))

    def __init__(self, data, rating=0, limit=999999999999999):
        RateModule.Rate.__init__(self, rating, limit)
        self.data = data
        self.id_tweet = self.get_id()

    def get_screen_name(self):
        return self.data['user']['screen_name']

    def get_retweeted_screen_name(self):
        try:
            return self.data['retweeted_status']['user']['screen_name']
        except KeyError:
            return None

    def get_text(self):
        return self.data['text']

    def get_id(self):
        return self.data['id']

    def is_retweeted(self):
        try:
            self.data['retweeted_status']['user']['screen_name']
        except KeyError:
            return False
        else:
            return True


def create_database():

    Base.metadata.create_all()
    Session = sessionmaker(bind=engine)
    s = Session()

    # data = {"id": 1, "text": "initial", "user": {"screen_name": "initial"}}
    # u = Tweet(data)
    #
    # s.add_all([u])
    s.commit()

if __name__ == "__main__":
    create_database()






















